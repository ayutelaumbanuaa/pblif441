var pool = require('./databaseConfig.js');
var barangDB = {
    getbarang: function (callback) {
        pool.getConnection(function (err, conn) {
            if (err) {
                console.log(err);
                return callback(err, null);
            }
            else {
                console.log("Connected!");
                var sql = 'SELECT * FROM barang';
                conn.query(sql, function (err, result) {
                    conn.release();
                    if (err) {
                        console.log(err);
                        return callback(err, null);
                    } else {

                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    },
    addbarang: function (kodebarang,tanggalperoleh, merk, nup, koderuangan, keterangan, nfctag,  callback) {
        pool.getConnection(function (err, conn) {
            if (err) {
                console.log(err);
                return callback(err, null);
            }
            else {
                console.log("Connected!");
                var sql = 'insert into barang (kodebarang,tanggalperoleh, merk, nup, koderuangan, keterangan, nfctag) values (?,?,?,?,?,?,?)';
                conn.query(sql, [kodebarang,tanggalperoleh, merk, nup, koderuangan, keterangan, nfctag], function (err, result) {
                    conn.release();
                    if (err) {
                        console.log(err);
                        return callback(err, null);
                    } else {

                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    },
    getruangan: function (callback) {
        pool.getConnection(function (err, conn) {
            if (err) {
                console.log(err);
                return callback(err, null);
            }
            else {
                console.log("Connected!");
                var sql = 'SELECT * FROM ruangan order by koderuangan';
                conn.query(sql, function (err, result) {
                    conn.release();
                    if (err) {
                        console.log(err);
                        return callback(err, null);
                    } else {

                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    },
    getruanganbynfc: function (catid, callback) {
        pool.getConnection(function (err, conn) {
            if (err) {
            console.log(err);
            return callback(err, null);
            }
            else {
            console.log("Connected!");
            var sql = "SELECT * FROM ruangan where nfctagruangan = ? order by koderuangan";
            // var sql = 'SELECT * FROM barang where koderuangan = ?';
            conn.query(sql, [catid], function (err, result) {
                conn.release();
                if (err) {
                    console.log(err);
                    return callback(err, null);
                } else {

                    console.log(result);
                    return callback(null, result);
                }
            });
            }
        });
    },
    getbarangbyruangan: function (catid, callback) {
        pool.getConnection(function (err, conn) {
            if (err) {
            console.log(err);
            return callback(err, null);
            }
            else {
            console.log("Connected!");
            var sql = 'select * from barang a, kategoribarang b, ruangan c where a.kodebarang = b.kodebarang and a.koderuangan=c.koderuangan and a.koderuangan=?';
            // var sql = 'SELECT * FROM barang where koderuangan = ?';
            conn.query(sql, [catid], function (err, result) {
                conn.release();
                if (err) {
                    console.log(err);
                    return callback(err, null);
                } else {

                    console.log(result);
                    return callback(null, result);
                }
            });
            }
        });
    },
    deletebarang: function (catid, callback) {
        pool.getConnection(function (err, conn) {
            if (err) {
            console.log(err);
            return callback(err, null);
            }
            else {
            console.log("Connected!");
            var sql = 'select * from barang a, kategoribarang b, ruangan c where a.kodebarang = b.kodebarang and a.koderuangan=c.koderuangan and a.kodebarang=?';
            // var sql = 'SELECT * FROM barang where koderuangan = ?';
            conn.query(sql, [catid], function (err, result) {
                conn.release();
                if (err) {
                    console.log(err);
                    return callback(err, null);
                } else {

                    console.log(result);
                    return callback(null, result);
                }
            });
            }
        });
    },
    deletebarangbyid: function (catid, callback) {
        pool.getConnection(function (err, conn) {
            if (err) {
            console.log(err);
            return callback(err, null);
            }
            else {
            console.log("Connected!");
            var sql = 'delete from barang where idbarang = ?';
            // var sql = 'SELECT * FROM barang where koderuangan = ?';
            conn.query(sql, [catid], function (err, result) {
                conn.release();
                if (err) {
                    console.log(err);
                    return callback(err, null);
                } else {

                    console.log(result);
                    return callback(null, result);
                }
            });
            }
        });
    }

    
};
module.exports = barangDB
