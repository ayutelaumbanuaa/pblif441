var express = require('express');
var app = express();

var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false });
var jsonParser = bodyParser.json();

var path = require('path');

var cors = require("cors")
var cor = cors();
app.use(cor);
app.use(express.static(path.join(__dirname, "../public")));

var barang = require('../model/barang.js');
// http://bmn.aplikasibmn.com/api/barang
app.get('/api/barang', function (req, res) {
    barang.getbarang(function (err, result) {
        if (!err) {
            res.send(result);
        }
        else {
            console.log(err);
            res.status(500).send(err);
        }
    });
});

//get by kodebarang
app.get('/api/barang/:catid', function (req, res) {
    var catid = req.params.catid;
    barang.getbarangbyid(catid, function (err, result) {
        if (!err) {
            res.send(result);
        }
        else {
            console.log(err);
            res.status(500).send(err);
        }
    });
});

//delete by kodebarang
app.delete('/api/barang/:catid', function (req, res) {
    var catid = req.params.catid;
    barang.deletebarangbyid(catid, function (err, result) {
        if (!err) {
            res.send(result);
        }
        else {
            console.log(err);
            res.status(500).send(err);
        }
    });
});


// http://bmn.aplikasibmn.com/api/barang/ruangan/702
app.get('/api/barang/ruangan/:catid', function (req, res) {
    var catid = req.params.catid;
    barang.getbarangbyruangan(catid, function (err, result) {
        if (!err) {
            res.send(result);
        }
        else {
            console.log(err);
            res.status(500).send(err);
        }
    });
});


app.post('/api/barang', urlencodedParser, jsonParser, function (req, res) {
    var kodebarang = req.body.kodebarang;
    var tanggalperoleh = req.body.tanggalperoleh;
    var merk = req.body.merk;
    var nup = req.body.nup;
    var koderuangan = req.body.koderuangan;
    var keterangan = req.body.keterangan;
    var nfctag = req.body.nfctag;

    barang.addbarang(kodebarang, tanggalperoleh, merk, nup, koderuangan, keterangan, nfctag, function (err, result) {
        if (!err) {
            res.send(result);
        }
        else {
            console.log(err);
            res.status(500).send(err);
        }
    });
});

// http://bmn.aplikasibmn.com/api/ruangan
app.get('/api/ruangan', function (req, res) {
    barang.getruangan(function (err, result) {
        if (!err) {
            res.send(result);
        }
        else {
            console.log(err);
            res.status(500).send(err);
        }
    });
});

// http://bmn.aplikasibmn.com/api/ruangan/barang/454094cc
app.get('/api/ruangan/nfc/:catid', function (req, res) {
    var catid = req.params.catid;
    barang.getruanganbynfc(catid, function (err, result) {
        if (!err) {
            res.send(result);
        }
        else {
            console.log(err);
            res.status(500).send(err);
        }
    });
});

module.exports = app
