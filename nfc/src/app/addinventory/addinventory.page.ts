import { Component, OnInit } from '@angular/core';
import { InventoryService } from '../services/inventory.service';
import { UtilsService } from '../services/utils.service';
import { Router } from '@angular/router';
import { NFC } from '@ionic-native/nfc/ngx';

@Component({
  selector: 'app-addinventory',
  templateUrl: './addinventory.page.html',
  styleUrls: ['./addinventory.page.scss'],
})
export class AddinventoryPage implements OnInit {
  kodeBarang: any;
  kodeRuangan: any;
  kodebaru: any;
  data = {
    idbarang: '',
    kodebarang: '',
    tanggalperoleh: '',
    merk: '',
    nup: '',
    koderuangan: '',
    keterangan: '',
    nfctag: ''
  };

  constructor(
    public inventoryService: InventoryService,
    public utils: UtilsService,
    public router: Router,
    public nfc: NFC
  ) {
    this.getKodeBarang();
    this.getRuangan();
    this.data.nfctag = this.utils.nfctag;
  }

  ngOnInit() { }
  getKodeBarang() {
    this.inventoryService.getKodeBarang().subscribe(data => {
      this.kodeBarang = data;
      console.log(data)
    })
  }
  getRuangan() {
    this.inventoryService.getRuangan().subscribe(data => {
      this.kodeRuangan = data;
      console.log(data)
    })
  }
  submit() {
    this.inventoryService.addInventory(this.data).subscribe(data => {
      this.utils.presentToast("success");
      this.router.navigate([''])
    });
  }
}
