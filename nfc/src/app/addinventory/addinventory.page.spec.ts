import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddinventoryPage } from './addinventory.page';

describe('AddinventoryPage', () => {
  let component: AddinventoryPage;
  let fixture: ComponentFixture<AddinventoryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddinventoryPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddinventoryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
