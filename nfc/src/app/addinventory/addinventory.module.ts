import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddinventoryPageRoutingModule } from './addinventory-routing.module';

import { AddinventoryPage } from './addinventory.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddinventoryPageRoutingModule
  ],
  declarations: [AddinventoryPage]
})
export class AddinventoryPageModule {}
