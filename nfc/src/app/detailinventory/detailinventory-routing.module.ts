import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailinventoryPage } from './detailinventory.page';

const routes: Routes = [
  {
    path: '',
    component: DetailinventoryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailinventoryPageRoutingModule {}
