import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailinventoryPage } from './detailinventory.page';

describe('DetailinventoryPage', () => {
  let component: DetailinventoryPage;
  let fixture: ComponentFixture<DetailinventoryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailinventoryPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailinventoryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
