import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailinventoryPageRoutingModule } from './detailinventory-routing.module';

import { DetailinventoryPage } from './detailinventory.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailinventoryPageRoutingModule
  ],
  declarations: [DetailinventoryPage]
})
export class DetailinventoryPageModule {}
