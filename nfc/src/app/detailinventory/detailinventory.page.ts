import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UtilsService } from '../services/utils.service';
import { PopoverController, Events } from '@ionic/angular';
import { SettingComponent } from '../setting/setting.component';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-detailinventory',
  templateUrl: './detailinventory.page.html',
  styleUrls: ['./detailinventory.page.scss'],
})
export class DetailinventoryPage implements OnInit {
  item: any;
  constructor(public utils: UtilsService, public popoverCtrl: PopoverController,
    private events: Events) { }

  ngOnInit() {
    this.item = this.utils.datadetailinventory;
    this.utils.DELETE_ID_INVENTORY = this.item.idbarang;
    console.log(this.item)
    console.log(this.utils.DELETE_ID_INVENTORY);

  }
  async settings(ev: any) {
    const popover = await this.popoverCtrl.create({
      component: SettingComponent,
      event: ev,
      animated: true,
      showBackdrop: true
    });
    return await popover.present();
  }

  // async settingsPopover(ev: any) {
  //   const popover = await this.popoverCtrl.create({
  //     component: SettingComponent,
  //     event: ev,
  //     animated: true,
  //     showBackdrop: true
  // });
  // return await popover.present();
  // }
}
