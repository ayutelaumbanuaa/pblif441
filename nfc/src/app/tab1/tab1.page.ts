import { Component } from '@angular/core';
import { NFC, Ndef } from '@ionic-native/nfc/ngx';
import { NavController, Platform, AlertController, ToastController, ModalController } from '@ionic/angular';
import { InventoryService } from '../services/inventory.service';
import { Response } from '@angular/http';
import { UtilsService } from '../services/utils.service';
import { AddinventoryPage } from '../addinventory/addinventory.page';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  tagId: string;
  data: any;
  rawdata: any;
  inventory: any;
  // nfctag: any = "46333814";
  nfctag: any;
  isruangan: boolean = false;

  constructor(
    public platform: Platform,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    public navCtrl: NavController,
    private nfc: NFC,
    private ndef: Ndef,
    public inventoryService: InventoryService,
    public utils: UtilsService,
    public modalCtrl: ModalController,
    public router: Router
  ) {
    this.platform.ready().then(() => {
      this.utils.checkconnection();
      this.addListenNFC();
    });
  }
  async presentConfirm() {
    const alert = await this.alertCtrl.create({
      header: "Data tidak ditemukan",
      message: "Apakah mau didaftarkan?",
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'OK',
          handler: () => {
            this.utils.navigate('addinventory');
          }
        }
      ]
    });
    await alert.present();
  }
  addinventory() {
    this.utils.navigate('addinventory');
  }
  removeInventory() {
    this.inventory = [];
  }
  removebarang() {
    this.inventory = null;
  }

  addListenNFC() {
    this.utils.presentToast("Listening to NFC...");
    this.nfc.addTagDiscoveredListener().subscribe((event) => {
      this.utils.presentToast("NFC Tag detected");
      this.nfctag = this.nfc.bytesToHexString(event.tag.id);
      this.utils.setnfc(this.nfctag);
      this.isruangan = false;
      if (this.utils.pagename == this.utils.PAGE_SCAN_INVENTORY) {
        // working from here ....
        if (this.utils.isconnected == true) {
          this.inventoryService.getbarangbynfc(this.nfctag)
            .subscribe(data => {
              // if (data.status == "fail") {
              //   this.inventory = null;
              //   if (this.utils.isconnected == true) {
              //     this.inventoryService.getroombynfc(this.nfctag)
              //       .subscribe(data => {
              //         {
              if (data.length == 0) {
                // this.presentConfirm();
                this.utils.presentAlert('NFC ini belum terdaftar');
              } else {
                // this.isruangan = false;
                this.inventory = data;
                var keterangan = data[0].keterangan;
                var ruangan = data[0].koderuangan;
                this.utils.presentAlert('NFC ini sudah terdaftar sebagai ' + keterangan + ' Di ruangan ' + ruangan + '.');
                // this.utils.presentAlert('NFC ini sudah terdaftar');
              }
              //         }
              //       })
              //   };
              //   // this.presentConfirm();
              // } else {
              //   this.inventory = data;
              //   this.utils.presentToast('Terdapat data');
              // }
            },
              error => { "Something went error..." }),
            (err) => this.utils.presentAlert("Please check your connection:"),
            () => { this.utils.presentAlert("Please check your connection") }
        } else {
          this.nfctag = null;
          // this.utils.presentToast("No connection");
        }
      }
    });

    this.nfc.addNdefListener().subscribe(() => { this.utils.presentAlert("addNdefListener working"); });
  }
  detail(item) {
    this.utils.datadetailinventory = item;
    this.router.navigate(['/detailinventory'])
  }
  ionViewWillEnter() {
    this.nfctag = null;
    this.utils.pagename = this.utils.PAGE_SCAN_INVENTORY;
    console.log(this.utils.pagename);
  }
}
