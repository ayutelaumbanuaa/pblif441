import { Injectable } from '@angular/core';
import { UtilsService } from './utils.service';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable({
  providedIn: 'root'
})
export class InventoryService {
  // mainUrl: any = "http://localhost/bmn/api/"
  mainUrl: any = "https://rwtest.aplikasibmn.com/api/";

  constructor(public http: Http, public utils: UtilsService) { }
  getInventories() {
    return this.http.get(this.mainUrl + 'barang').map(res => res.json());
  }
  getInventory(kodeBarang) {
    return this.http.get(this.mainUrl + 'barang?id=' + kodeBarang).map(res => res.json());
  }
  deleteInventorybyid(id) {
    return this.http.delete(this.mainUrl + 'barang/' + id).map(res => res.json());
  }
  getRuangan() {
    return this.http.get(this.mainUrl + 'ruangan').map(res => res.json());
  }
  getKodeBarang() {
    return this.http.get(this.mainUrl + 'kodebarang').map(res => res.json());
  }
  addInventory(data) {
    return this.http.post(this.mainUrl + 'barang', data).map(res => { res.json(); });
  }
  getInventorybyRoom(data) {
    this.utils.presentToast('Ruangan' + data);
    return this.http.get(this.mainUrl + 'barang/ruangan/' + data).map(res => res.json());
  }
  getroombynfc(data) {
    return this.http.get(this.mainUrl + 'ruangan/nfc/' + data).map(res => res.json());
  }
  getbarangbynfc(data) {
    return this.http.get(this.mainUrl + 'barang/nfc/' + data).map(res => res.json());
  }
}

//sampah
    // return this.http.post('http://localhost/api/user/create.php', body).map(res => { res.json(); });
    // let headers = new Headers();
    // headers.append('Content-Type', 'Application/json');
    // headers.append('Content-Type', 'application/x-www-form-urlencoded');
    // headers.append('Access-Control-Allow-Origin', '*');
    // headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    // headers.append('Accept', 'application/json');
    // headers.append('content-type', 'application/json');
    // let options = new RequestOptions({ headers: headers });
    // console.log(data);
    // console.log(headers);
    // let body = {
    //   "kodebarang": '117174'// change to "card"
    // }
    // let body = {
    //   "email": "email@gmail.com",// change to "card"
    //   "password": "pass"// change to "card"
    // }

    // console.log(body)
    // let url = this.mainUrl + 'users';
    // console.log(url);

    // return this.http.post('http://localhost/ci-restserver-master/index.php/api/Example/users', body, { headers: headers }).map(res => res.json());
    // return this.http.post('http://aplikasibmn.com/api/barang', data, options).map(res => {
    //   res.json();
    //   this.utils.presentToast('Successful');
    // });
