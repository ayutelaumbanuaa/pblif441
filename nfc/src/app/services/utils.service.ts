import { Injectable } from '@angular/core';
import { ToastController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { Network } from '@ionic-native/network/ngx';


@Injectable({
  providedIn: 'root'
})
export class UtilsService {
  nfctag: any;
  isconnected: any = false;
  connectSubscription: any;
  disconnectSubscription: any;
  appnoconnection = 'No connection';
  appbacktoonline = 'Back online';
  public itemssearch: any = [];
  datadetailinventory: any;
  pagename: any;
  PAGE_SCAN_INVENTORY = "tab1";
  PAGE_LIST_INVENTORY = "tab2";
  koderuangan: any = ""
  DELETE_ID_INVENTORY: any = "";




  constructor(
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public router: Router,
    public storage: Storage,
    public network: Network
  ) { }
  filterItems(searchTerm) {
    return this.itemssearch.filter(item => {
      return item.title.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
    });
  }
  navigate(data) {
    this.router.navigate(['/' + data]);
  }
  async presentToast(message) {
    const toast = await this.toastCtrl.create({
      message: message,
      duration: 1000,
      position: 'bottom'
    });
    toast.present();
  }
  async presentAlert(message) {
    const alert = await this.alertCtrl.create({
      header: 'Information',
      message: message,
      buttons: ['OK']
    });
    await alert.present();
  }
  setnfc(data) {
    this.nfctag = data;
  }
  checkconnection() {
    // watch network for a connection
    this.connectSubscription = this.network.onConnect().subscribe(() => {
      // this.presentToast(this.appbacktoonline);
      this.isconnected = true;
      // We just got a connection but we need to wait briefly
      // before we determine the connection type. Might need to wait.
      // prior to doing any api requests as well.
      // setTimeout(() => {
      //   if (this.network.type === 'wifi') {
      //     console.log('we got a wifi connection, woohoo!');
      //   }
      // }, 3000);
    });
    // watch network for a disconnection
    this.disconnectSubscription = this.network.onDisconnect().subscribe(() => {
      this.presentToast(this.appnoconnection);
      this.isconnected = false;
    });
  }

  removeconnection() {
    // stop connect watch
    this.connectSubscription.unsubscribe();
    // stop disconnect watch
    this.disconnectSubscription.unsubscribe();
  }

}
