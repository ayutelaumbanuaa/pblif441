export interface Inventory {
    idbarang: number,
    kodebarang: number,
    tanggalperoleh: Date,
    merk: string,
    nup: number
    koderuangan: string,
    keterangan: string,
    nfctag: string
}