import { Component } from '@angular/core';
import { InventoryService } from '../services/inventory.service';
import { UtilsService } from '../services/utils.service';
import { NFC } from '@ionic-native/nfc/ngx';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  masterdata: any;
  items: any;
  kodeRuangan: any;
  // ruanganselected: any = "454094cc";
  ruanganselected: any = "";
  searchTerm: string = "";

  constructor(
    public inventoryService: InventoryService,
    public utils: UtilsService,
    public nfc: NFC,
    public router: Router) {
    this.getRuangan();
    this.getData()
    this.addListenNFC();
  }
  getData() {
    this.items = [];
    this.masterdata = [];
    this.searchTerm = "";
    if (this.ruanganselected == "") {
      this.inventoryService.getInventories().subscribe(data => {
        this.masterdata = data;
        this.items = this.masterdata;
      })
    } else {
      this.inventoryService.getInventorybyRoom(this.ruanganselected).subscribe(data => {
        this.masterdata = data;
        this.items = this.masterdata;
      })
    }
  }
  getRuangan() {
    this.inventoryService.getRuangan().subscribe(data => {
      this.kodeRuangan = data;
      console.log(data)
    })
  }
  //alur kerja : baca tag nfc > check page = inventory > ambil nfc tag > kirim ke server >
  // dapat kode ruangan > set ruanganselected > getdata()

  addListenNFC() {
    this.nfc.addTagDiscoveredListener().subscribe((event) => {
      if (this.utils.pagename == this.utils.PAGE_LIST_INVENTORY) {
        if (this.utils.isconnected == true) {
          if (this.kodeRuangan.length == 0) {
            this.getRuangan();
          }
          // let tag = '454094cc';
          let tag = this.nfc.bytesToHexString(event.tag.id)
          this.inventoryService.getroombynfc(tag)
            .subscribe(data => {
              if (data.status == "fail") {
                this.items = null;
              } else {
                this.ruanganselected = data[0].koderuangan;
                this.getData();
              }
            },
              error => { "Something went error..." }),
            (err) => this.utils.presentAlert("Please check your connection:"),
            () => { this.utils.presentAlert("Please check your connection") }
        } else {
          this.utils.presentToast("No connection, pull down for refresh the data!");
        }
      }

      // this.ruanganselected = 
      //  if (this.utils.isconnected == true) {
      //   this.inventoryService.getInventorybyRoom(this.ruanganselected)
      //     .subscribe(data => {
      //       if (data.status == "fail") {
      //         this.items = null;
      //       } else {
      //         this.items = data;
      //       }
      //     },
      //       error => { "Something went error..." }),
      //     (err) => this.utils.presentAlert("Please check your connection:"),
      //     () => { this.utils.presentAlert("Please check your connection") }
      // } else {
      //   this.utils.presentToast("No connection");
      // }
    });
  }

  doRefresh(event) {
    this.getRuangan();
    this.getData();
    console.log('Begin async operation');
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 1000);
  }

  setFilteredItems() {
    this.items = this.masterdata.filter(item => {
      return item.namabarang.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1 || item.nfctag.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1 || item.keterangan.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1;
    });
    console.log(this.items);
  }
  detail(item) {
    this.utils.datadetailinventory = item;
    this.router.navigate(['/detailinventory'])
  }
  ionViewWillEnter() {
    this.utils.pagename = this.utils.PAGE_LIST_INVENTORY;
    console.log(this.utils.pagename);
    this.getData();
  }

}