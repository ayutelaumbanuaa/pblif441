import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'addinventory',
    loadChildren: () => import('./addinventory/addinventory.module').then(m => m.AddinventoryPageModule)
  },
  {
    path: 'detailinventory',
    loadChildren: () => import('./detailinventory/detailinventory.module').then(m => m.DetailinventoryPageModule)
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
