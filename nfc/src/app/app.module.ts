import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy, PopoverController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NFC, Ndef } from '@ionic-native/nfc/ngx';
import { HttpModule } from '@angular/http'
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { IonicStorageModule } from '@ionic/storage'
import { registerLocaleData } from '@angular/common';
import localeId from '@angular/common/locales/id';
registerLocaleData(localeId, 'id');
import { Network } from '@ionic-native/network/ngx';
import { SettingComponent } from './setting/setting.component'

@NgModule({
  declarations: [AppComponent, SettingComponent],
  entryComponents: [SettingComponent],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    IonicStorageModule.forRoot()],
  providers: [
    Network,
    NFC,
    Ndef,
    StatusBar,
    SplashScreen,
    { provide: LOCALE_ID, useValue: "id-ID" },
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
