import { Component, OnInit } from '@angular/core';
import { UtilsService } from '../services/utils.service';
import { PopoverController, AlertController } from '@ionic/angular';
import { InventoryService } from '../services/inventory.service';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss'],
})
export class SettingComponent implements OnInit {
  constructor(
    public utils: UtilsService,
    public popoverCtrl: PopoverController,
    public inventoryService: InventoryService,
    public alertCtrl: AlertController
  ) { }

  ngOnInit() { }
  async onDismiss() {
    try {
      await this.popoverCtrl.dismiss();
    } catch (e) {
      //click more than one time popover throws error, so ignore...
    }
  }

  delete() {
    this.utils.presentAlert('delete item');
    let id = this.utils.DELETE_ID_INVENTORY;
    this.inventoryService.deleteInventorybyid(id).subscribe(data => {
      console.log(this.utils.DELETE_ID_INVENTORY);
      console.log(id);
      console.log(data);
      console.log('delete');
    });
    this.onDismiss();
    this.utils.navigate('')
  }
  async deleteConfirm() {
    const alert = await this.alertCtrl.create({
      header: "Menghapus data",
      message: "Apakah anda yakin akan menghapus data ini?",
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'OK',
          handler: () => {
            this.delete();
          }
        }
      ]
    });
    await alert.present();
  }

}
